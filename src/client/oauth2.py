import requests


class Crendentials(object):
    def __init__(self):
        self._access_token = None
        self._refresh_token = None

    @property
    def access_token(self):
        return self._access_token

    def refresh(self):
        pass


class OAuth2Crendentials(Crendentials):
    def __init__(self, config):
        super(OAuth2Crendentials, self).__init__()
        self._refresh_url = config.get('refresh_url')
        self._refresh_token = config.get('refresh_token')
        self._access_token = config.get('access_token')
        self._client_id = config.get('client_id')
        self._client_secret = config.get('client_secret')

    def refresh(self):
        req_data = {
            'grant_type': 'refresh_token',
            'refresh_token': self._refresh_token,
            'client_id': self._client_id,
            'client_secret': self._client_secret
        }
        resp = requests.post(self._refresh_url, data=req_data)
        resp.raise_for_status()
        result = resp.json()
        self._access_token = result.get('access_token')
