#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import requests
import argparse
import json
from os.path import basename, splitext

from importer import Importer
from oauth2 import OAuth2Crendentials
from dao import JsonFileDAO
from hotzone_client import HotzoneClient

LOG = logging.getLogger(__name__)


SITENAMES = [
            'twn-h1',
            'twn-h2',
            'twn-h3',
            'twn-h4',
            'twn-h5',
            'twn-h6',
            'twn-h7',
            'twn-h8',
            ]


def get_bs_id(site, bs_name, auth_conf, cred):
    client = HotzoneClient(credentials=cred, site=site,
                           config={'connection': {'max_retry_num': 7}})
    return client.get_bs_id(bs_name)


if __name__ == '__main__':
    logging.getLogger("requests").setLevel(logging.WARNING)
    requests.packages.urllib3.disable_warnings()

    parser = argparse.ArgumentParser(description='tphotzone batch post client')
    parser.add_argument('-a', '--auth_prefix', metavar='<auth_prefix>',
                        type=str, required=True,
                        help=('a prefix of file in json contains tokens'))
    parser.add_argument('-f', '--datafile', metavar='datafile',
                        type=str, required=True,
                        help=('資料來源檔'))
    parser.add_argument('-d', '--datadate', metavar='datadate',
                        type=str, required=True,
                        help=('資料日期'))
    parser.add_argument('-b', '--bs_name', metavar='bs_name',
                        type=lambda s: unicode(s, 'utf8'), required=True,
                        help=('看板名'))
    parser.add_argument('-l', '--logfile', metavar='logfile',
                        type=str, required=True,
                        help=('log file'))
    args = parser.parse_args()

    config = {
              'connection': {'max_retry_num': 7}
             }
    datafile = args.datafile
    # e.g. u'/tmp/台北市自行車竊盜點位資訊.json'
    dataname = splitext(basename(args.datafile))[0]
    # e.g. u'台北市自行車竊盜點位資訊'
    datadate = args.datadate
    # e.g. u'2015.10.17'
    auth_prefix = args.auth_prefix
    # e.g. 'auth/auth.conf.'
    bs_name = args.bs_name
#    bs_name = u'易發生婦幼犯罪被害地點'
    # e.g. '易發生婦幼犯罪被害地點'
    logging.basicConfig(level=logging.INFO,
                        filename=args.logfile,
                        )
    for sitename in SITENAMES:
        site = "https://{0}.appspot.com/".format(sitename)
        auth_file = auth_prefix + sitename
        with open(auth_file) as fp:
            auth_config = json.loads(fp.read())
        cred = OAuth2Crendentials(auth_config)
        bs_id = get_bs_id(site, bs_name, auth_config, cred)
        print 'Importing', site
        LOG.info('Importing')
        LOG.info(dataname)
        LOG.info(datadate)
        LOG.info(site)
        LOG.info(bs_name)
        LOG.info(bs_id)
        LOG.info(auth_file)
        dao = JsonFileDAO(datafile)
        importer = Importer(config=config, datadate=datadate,
                            dataname=dataname, dao=dao, bs_id=bs_id,
                            credentials=cred, site=site)
        importer.post_data()
