import argparse

URL = "https://accounts.google.com/o/oauth2/auth?scope={scope}&redirect_uri={redirect_uri}&response_type=code&client_id={client_id}&access_type=offline&approval_prompt=force"

if __name__ == '__main__':
    # step 1:
    # python src/client/auth_utils/gen_get_code_url.py -c <client_id>
    # step 2:
    # paste it to your browser
    # step 3:
    # python src/client/auth_utils/get_token.py -c <client_id> -s <secret> -k <code> > auth.conf.twn-h999
    parser = argparse.ArgumentParser(description=('gen get code url, use this tool'
                                                  ' to get code and pass it to get_token to get token'))
    parser.add_argument('-s', '--scope', metavar='<scope>', default="email",
                        type=str, required=False, help=('scope'))
    parser.add_argument('-r', '--redirect_uri', metavar='<redirect_uri>',
                        default="http://localhost:9004",
                        type=str, required=False, help=('redirect_uri'))
    parser.add_argument('-c', '--client_id', metavar='<client_id>',
                        type=str, required=True, help=('client_id'))
    args = parser.parse_args()

    print URL.format(scope=args.scope, redirect_uri=args.redirect_uri,
                     client_id=args.client_id)
