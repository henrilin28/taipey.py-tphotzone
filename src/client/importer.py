#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import requests
from requests import Session
import json
import argparse
from os.path import basename, splitext
from functools import wraps

from hotzone_client import HotzoneClient
from oauth2 import OAuth2Crendentials
from dao import JsonFileDAO


LOG = logging.getLogger(__name__)


class Importer(object):
    def __init__(self, *args, **kwargs):
        self.dao = kwargs.get('dao')
        self.datadate = kwargs.get('datadate')
        self.dataname = kwargs.get('dataname')
        self.bs_id = kwargs.get('bs_id')
        cred = kwargs.get('credentials')
        self.hotzone_client = HotzoneClient(site=kwargs.get('site'),
                                            credentials=cred,
                                            config=kwargs.get('config'))

    def post_data(self):
        while True:
            try:
                item = self.dao.next()
                item['attributes'].append({u'name': u'資料來源日期',
                                           u'value': self.datadate.decode('utf-8')})
                item['attributes'].append({u'name': u'資料來源名稱',
                                           u'value': self.dataname.decode('utf-8')})
                if item['location'] is not None:
                    self.hotzone_client.create_item(self.bs_id, item)
                    LOG.info('put ok!')
                    LOG.info(json.dumps(item, ensure_ascii=False, indent=2))
                else:
                    LOG.error('no location, please put it manually')
                    LOG.error(json.dumps(item, ensure_ascii=False, indent=2))
            except StopIteration:
                break


if __name__ == '__main__':
    logging.getLogger("requests").setLevel(logging.WARNING)
    requests.packages.urllib3.disable_warnings()
    parser = argparse.ArgumentParser(description='tphotzone client')
    parser.add_argument('-a', '--auth_config', metavar='<config>',
                        type=str, required=True,
                        help=('a file in json contains tokens'))
    parser.add_argument('-f', '--datafile', metavar='datafile',
                        type=str, required=True,
                        help=('資料來源檔'))
    parser.add_argument('-d', '--datadate', metavar='datadate',
                        type=str, required=True,
                        help=('資料日期'))
    parser.add_argument('-b', '--bs_id', metavar='bs_id',
                        type=str, required=True,
                        help=('看板id'))
    parser.add_argument('-l', '--logfile', metavar='logfile',
                        type=str, required=True,
                        help=('log file'))
    parser.add_argument('-s', '--site', metavar='site',
                        type=str, required=True,
                        help=('site'))
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO,
                        filename=args.logfile,
                        )
    config = {
              'connection': {'max_retry_num': 7}
             }
    datafile = args.datafile
    # e.g. u'/tmp/台北市自行車竊盜點位資訊.json'
    dataname = splitext(basename(args.datafile))[0]
    # e.g. u'台北市自行車竊盜點位資訊'
    datadate = args.datadate
    # e.g. u'2015.10.17'
    auth_file = args.auth_config
    # e.g. 'auth.conf'
    bs_id = args.bs_id
    # e.g. '5066549580791808'
    site = args.site
    with open(auth_file) as fp:
        auth_config = json.loads(fp.read())
    dao = JsonFileDAO(datafile)
    cred = OAuth2Crendentials(auth_config)
    importer = Importer(config=config, datadate=datadate, dataname=dataname,
                        dao=dao, bs_id=bs_id,
                        credentials=cred, site=site)
    importer.post_data()
