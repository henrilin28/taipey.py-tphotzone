var google = google || {};

google.appengine = google.appengine || {};

google.appengine.esbs = google.appengine.esbs || {};

google.appengine.esbs.init = function(apiRoot) {
    var apisToLoad;
    var callback = function() {
    }
    gapi.client.load('esbs', 'v1.0', callback, apiRoot);
};
