#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import codecs
import json
import argparse
from os.path import basename, splitext, dirname, join
import io
from collections import OrderedDict
import logging
import uuid

import requests

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')

LOG = logging.getLogger(__name__)


class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")


class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self


def addr2loc(address):
    url = ('https://maps.google.com/maps/api/geocode/json?address='
               + address + '=&sensor=false')
    resp = requests.get(url=url)
    if resp.json()['status'] == 'OK':
        location = resp.json()['results'][0]['geometry']['location']
        return "%s,%s" % (location['lat'], location['lng'])
    else:
        return None


class Csv2JsonConverter(object):
    def __init__(self, csv_file, name_index, address_index, output_file):
        self.csv_file = csv_file
        self.name_index = name_index
        self.address_index = address_index
        self.output_file = output_file

    def convert(self):
        items = []
        with open(self.csv_file, 'rb') as f:
            reader = UnicodeReader(f)
            header = reader.next()
            length = len(header)
            for row in reader:
                item = OrderedDict()
                item['attributes'] = []
                for i in range(0, length):
                    item['attributes'].append({'name': header[i],
                                               'value': row[i]})
                item[u'id'] = str(uuid.uuid4()).replace('-', '')
                item[u'name'] = row[self.name_index]
                address = row[self.address_index]
                item[u'location'] = addr2loc(address)
                items.append(item)
                LOG.info(json.dumps(item, ensure_ascii=False, indent=2))
        with io.open(self.output_file, 'w', encoding='utf8') as fp:
            json_str = json.dumps(items, ensure_ascii=False, indent=2)
            fp.write(json_str)


if __name__ == '__main__':
    logging.getLogger("requests").setLevel(logging.WARNING)
    requests.packages.urllib3.disable_warnings()
    parser = argparse.ArgumentParser(description='csv data to json converter')
    parser.add_argument('-c', '--csv_file', metavar='<csv_file>',
                        type=str, required=True,
                        help=('csv file dataset'))
    parser.add_argument('-n', '--name_index', metavar='<name_index>',
                        type=int, required=True,
                        help=('index number of name column'))
    parser.add_argument('-a', '--address_index', metavar='<address_index>',
                        type=int, required=True,
                        help=('index number of address column'))
    args = parser.parse_args()
    csv_file = args.csv_file
    name_index = args.name_index
    address_index = args.address_index
    output_dir = dirname(csv_file)
    filename = splitext(basename(args.csv_file))[0] + '.json'
    output_file = join(output_dir, filename)
    print 'Output:'
    print output_file
    converter = Csv2JsonConverter(csv_file, name_index, address_index,
                                  output_file)
    converter.convert()
