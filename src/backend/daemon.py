import endpoints

import lib_path

from bs.api import BrokerSystemApi
from item.api import ItemApi

apis = [BrokerSystemApi, ItemApi]
APPLICATION = endpoints.api_server(apis)
