from google.appengine.ext import ndb
from google.appengine.ext.ndb import msgprop

from ..bs.model import BrokerSystem
from .message import Attribute


class Item(ndb.Model):
    broker_system = ndb.KeyProperty(kind=BrokerSystem)
    name = ndb.StringProperty()
    location = ndb.GeoPtProperty()
    attributes = msgprop.MessageProperty(Attribute, repeated=True)
    ctime = ndb.DateTimeProperty(auto_now_add=True)
    mtime = ndb.DateTimeProperty(auto_now=True)
