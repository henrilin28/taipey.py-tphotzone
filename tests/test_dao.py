#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import json
import os
import io

from client.dao import JsonFileDAO


class Test(unittest.TestCase):
    def setUp(self):
        self.jsonfile = "/tmp/test_data.json"

    def tearDown(self):
        os.unlink(self.jsonfile)

    def test_json_dao(self):
        item1 = {
           u'name': u'自行車竊盜',
           u'案類': u'自行車竊盜',
           u'發生(現)日期': u'1040104',
           u'發生時段': u'07~09',
           u'發生(現)地點': u'台北市北投區清江里三合街二段481-510號',
           u'address': u'台北市北投區清江里三合街二段481-510號'
        }
        item2 = {
           u'name': u'自行車竊盜',
           u'案類': u'自行車竊盜',
           u'發生(現)日期': u'1040101',
           u'發生時段': u'16~18',
           u'發生(現)地點': u'台北市松山區撫遠街361-390號',
           u'address': u'台北市松山區撫遠街361-390號'
          }
        src = [item1, item2]

        with io.open(self.jsonfile, 'w', encoding='utf8') as fp:
            json_str = json.dumps(src, ensure_ascii=False, indent=2)
            fp.write(json_str)

        dao = JsonFileDAO(self.jsonfile)
        assert item1 == dao.next()
        assert item2 == dao.next()
        self.assertRaises(StopIteration, dao.next)
