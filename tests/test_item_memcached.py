import unittest
import cPickle as pickle

from item.memcached import (set_memcache, get_memcache, split_into_chunks,
                            merge_chunks)

from mockcache import Client


class ClientWrapper(object):
    def __init__(self):
        self._client = Client()
        self.MAX_VALUE_SIZE = 16

    def __getattr__(self, attr):
        if attr in self.__dict__:
            return getattr(self, attr)
        return getattr(self._client, attr)


class TestMemcache(unittest.TestCase):
    def setUp(self):
        self.client = ClientWrapper()
        self.bs_id = '3345678'

    def test_set_memcache_greater_max_value_size(self):
        items = 'a' * 17
        set_memcache(self.client, self.bs_id, items)
        data = get_memcache(self.client, self.bs_id)
        assert data == items

    def test_set_memcache_less_max_value_size(self):
        items = 'a' * 1
        set_memcache(self.client, self.bs_id, items)
        data = get_memcache(self.client, self.bs_id)
        assert data == items

    def test_set_memcache_with_miss_chunk(self):
        items = 'a' * 30
        set_memcache(self.client, self.bs_id, items)
        # drop cache
        key = "%s_1" % self.bs_id
        self.client.set(key, None)
        data = get_memcache(self.client, self.bs_id)
        assert data == None


class TestSplit(unittest.TestCase):
    def test_split_and_merge_chunks(self):
        _dict = {}
        for i in range(100):
            _dict[str(i)] = i
        msg = pickle.dumps(_dict)
        chunks = split_into_chunks(msg, 16)
        ret = merge_chunks(chunks)
        assert ret == msg
