test_site_path = /tmp/tphotzone_test
SITES = twn-h1 twn-h2 twn-h3 twn-h4 twn-h5 twn-h6 twn-h7 twn-h8

run:
	rm -rf $(test_site_path)
	cp -r ./ $(test_site_path)
	sed -i 's/google_oauth.signin(mode, sign_in_cbk);/$$scope.signed_in = true;$$scope.email = "test@gmail.com";\ /g' $(test_site_path)/src/frontend/web/static/esbs/controller.js
	sed -i 's/if endpoints.get_current_user().email() not in MANAGERS:/if False:\ /g' $(test_site_path)/src/backend/utils.py
	sed -i 's/if not endpoints.get_current_user():/if False:\ /g' $(test_site_path)/src/backend/utils.py
	sed -i 's/email = endpoints.get_current_user().email()/email = "test@gmail.com"\ /g' $(test_site_path)/src/backend/item/api.py
	sed -i 's/if item.owner != endpoints.get_current_user().email():/if False:\ /g' $(test_site_path)/src/backend/item/api.py
	dev_appserver.py --log_level debug $(test_site_path)

deploy:
	cd ..; appcfg.py update tphotzone/

deployall:
	for site in $(SITES) ; do \
		echo deploy $$site; \
		git checkout $$site; \
		cd ..; appcfg.py update tphotzone/; \
		cd tphotzone/; \
	done
	git checkout master

mergeall:
	for site in $(SITES) ; do \
		echo merge $$site; \
		git checkout $$site; \
		git merge master; \
	done
	git checkout master

pushall:
	for site in $(SITES) ; do \
		echo push $$site; \
		git checkout $$site; \
		git push $$site $$site:master; \
	done
	git checkout master

testall:
	PYTHONPATH=./src/backend nosetests-2.7 tests -v -s -d --with-xunit --with-xcover
