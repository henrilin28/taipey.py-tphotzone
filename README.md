# README #

### Introduction ###
* Please visit our site: http://tw-hotzone.appspot.com
* This project do visualization for open data, which can be show on map, from the government.

### Contribution guidelines ###

* If you are not a core developer, you can not commit to master branch of this repository directly. However, you can fork it and make a merge request on Bitbucket. The code will be reviewed by core developers. If the commit is accepted and the patch or branch will be merged into the main branch by a core developer.
* After contributing more than 2 commits and if you want to join core developers, notify us and we will set the permission of this project for you.